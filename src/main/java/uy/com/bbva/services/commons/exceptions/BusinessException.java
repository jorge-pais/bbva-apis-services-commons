package uy.com.bbva.services.commons.exceptions;
public class BusinessException extends ServiceException {
	private String internCode;
	public BusinessException(String message, String internMessage, String internCode, Exception e) {
		super(message, internMessage, e);
		this.internCode = internCode;
	}
	public String getInternCode() {
		return internCode;
	}
	public void setInternCode(final String internCode) {
		this.internCode = internCode;
	}
}