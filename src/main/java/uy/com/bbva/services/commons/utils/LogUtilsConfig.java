package uy.com.bbva.services.commons.utils;
public class LogUtilsConfig {
    private static final String EXTERNAL_SEPARATOR = "#";
    private static final String INTERNAL_SEPARATOR = " | ";
    private static final String MESSAGE = "BBVA.MESSAGE: {}";
    private static final String INTERNAL_MESSAGE = "BBVA.INTERNAL_MESSAGE: {}";
    private static final String ERROR_CODE = "BBVA.ERROR_CODE: {}";
    private static final String TOTAL_ROWS = "BBVA.TOTAL_ROWS: {}";
    private static final String DURATION = "BBVA.DURATION: {} milliseconds";
    private static final String QUERY = "BBVA.QUERY: {}";
    private static final String COUNTRY = "BBVA.COUNTRY: {}";
    private static final String DOC_TYPE = "BBVA.DOC_TYPE: {}";
    private static final String DOC_NUMBER = "BBVA.DOC_NUMBER: {}";
    private static final String ACCOUNT_USED = "BBVA.ACCOUNT_USED: {}";
    private static final String USER = "BBVA.USER: {}";
    private static final String CODE = "BBVA.CODE: {}";
    private static final String TIME_REQUEST = "BBVA.TIME-REQUEST: {}";
    private static final String PAYLOAD_BUSINESS =
            EXTERNAL_SEPARATOR + LogType.BUSINESS.getValue()
                    + INTERNAL_SEPARATOR + MESSAGE
                    + INTERNAL_SEPARATOR + INTERNAL_MESSAGE
                    + INTERNAL_SEPARATOR + ERROR_CODE
                    + EXTERNAL_SEPARATOR;
    private static final String PAYLOAD_DEBUG =
            EXTERNAL_SEPARATOR + LogType.DEBUG.getValue()
                    + INTERNAL_SEPARATOR + MESSAGE
                    + INTERNAL_SEPARATOR + INTERNAL_MESSAGE
                    + EXTERNAL_SEPARATOR;
    private static final String PAYLOAD_ERROR =
            EXTERNAL_SEPARATOR + LogType.ERROR.getValue()
                    + INTERNAL_SEPARATOR + MESSAGE
                    + INTERNAL_SEPARATOR + INTERNAL_MESSAGE
                    + EXTERNAL_SEPARATOR;
    private static final String PAYLOAD_QUERY =
            EXTERNAL_SEPARATOR + LogType.QUERY.getValue()
                    + INTERNAL_SEPARATOR + QUERY
                    + INTERNAL_SEPARATOR + DURATION
                    + INTERNAL_SEPARATOR + TOTAL_ROWS
                    + EXTERNAL_SEPARATOR;
    private static final String PAYLOAD_LOGIN_ERROR =
            EXTERNAL_SEPARATOR + LogType.LOGIN.getValue()
                    + INTERNAL_SEPARATOR + COUNTRY
                    + INTERNAL_SEPARATOR + DOC_TYPE
                    + INTERNAL_SEPARATOR + DOC_NUMBER
                    + INTERNAL_SEPARATOR + ACCOUNT_USED
                    + INTERNAL_SEPARATOR + USER
                    + INTERNAL_SEPARATOR + CODE
                    + EXTERNAL_SEPARATOR;
    private static final String PAYLOAD_MONITOREO =
            EXTERNAL_SEPARATOR + LogType.MONITOREO.getValue()
                    + INTERNAL_SEPARATOR + TIME_REQUEST
                    + EXTERNAL_SEPARATOR;
    public static String getPayloadBusiness() {
        return PAYLOAD_BUSINESS;
    }
    public static String getPayloadDebug() {
        return PAYLOAD_DEBUG;
    }
    public static String getPayloadError() {
        return PAYLOAD_ERROR;
    }
    public static String getPayloadQuery() {
        return PAYLOAD_QUERY;
    }
    public static String getPayloadLoginError() {
        return PAYLOAD_LOGIN_ERROR;
    }
    public static String getPayloadMonitoreo() {
        return PAYLOAD_MONITOREO;
    }
}