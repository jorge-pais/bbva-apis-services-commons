package uy.com.bbva.services.commons.crypto;
	import java.math.BigInteger;
	import java.security.KeyFactory;
	import java.security.NoSuchAlgorithmException;
	import java.security.PrivateKey;
	import java.security.PublicKey;
	import java.security.spec.RSAPrivateCrtKeySpec;
	import java.security.spec.RSAPublicKeySpec;
	import javax.crypto.Cipher;
	import org.apache.commons.lang3.StringUtils;
	import org.bouncycastle.jce.provider.BouncyCastleProvider;
	import org.slf4j.LoggerFactory;
	public class CriptografiaRSA {
	    private static final int BYTES = 117;
	    private static CriptografiaRSA instance = null;
	    private static byte[] modulusBytes = null;
	    private static byte[] exponentBytes = null;
	    private static byte[] pBytes = null;
	    private static byte[] qBytes = null;
	    private static byte[] dPBytes = null;
	    private static byte[] dQBytes = null;
	    private static byte[] inverseQBytes = null;
	    private static byte[] dBytes = null;
	    private static BigInteger modulus = null;
	    private static BigInteger exponent = null;
	    private static KeyFactory fact = null;
	    private static Cipher cipher = null;
	    public static final String MODULUS_CLAVE = "lLtMDWVvBUbtnv/wLYTlgRL/TZ+fKNAgqMwNToRbPSel4OirmYYMdFyIKrVEVXZlTTviBdy7omfFFedjRABPRnhVPkZ9afq/Abb8cOgCfh9MZlmM9unFrN31xHAgy3c9H+MYyWAXqBhQsNYuKrHPAQm1nRUmzaAWTkwBIDof1sU=";
	    public static final String EXPONENT_CLAVE = "AQAB";
	    public static final String P_BYTES_CLAVE = "xgLE+uZyj5+JYqpREMYd3fIrcp0dQ9ew+szxEAJdfn3J3VCvF8TGst3rdSnZHFxa3cbV9P3Yp7NSA4YqgdC4FQ==";
	    public static final String Q_BYTES_CLAVE = "wEn6NCmI7bVQNykJv5EfcxTRECF1I4rQlrLNlLmcY2x4G313eLAbnGnI/+BBRIcfxadLBuo5KpnaBd/MQvgf8Q==";
	    public static final String DP_BYTES_CLAVE = "aSrfZEMxZYbeeUDsfXQ8OL1+xgkcmgPdQ9fF0TLwZVZ5+Mn5tS4WOroTbM8lOSL0QAZzbQPGJovfa2++8+PfYQ==";
	    public static final String DQ_BYTES_CLAVE = "a+rY1l+dLrO8okiQ5Qegm+jJ8ICmil0E9h7BjD+jipU8z0jaPUyxIlaobRrB5qDjbv2V+PqJPOR93SAeNlOlMQ==";
	    public static final String INVERSE_BYTES_CLAVE = "VAZ36r03NPe9jtR6xKGA4JJvCzC62xmLh1AD2Bk4NlN1daTqgrj4G1zwxsbh0btTkZ16RsLuOLU8CrJqnVHejQ==";
	    public static final String D_BYTES_CLAVE = "gyMBU250DPsU2HRhmEDs+nl9kTuEYb/gi/QeZ3Rb3ffvUfbUFnES2iYcCUv6DEBv4UFDcFfHONK1+zIhW/vJ6MhepXBKlHkP+H59k3UbEfDNUxyKriQovCyWg1Vqd0Bp6DCDzjm1Ex7GRYCBA8HVaJMVxF2d6ojvJr4DgPJs2gE=";
	    public synchronized static CriptografiaRSA getInstance() {
	        if (instance != null) {
	            return instance;
	        } else {
	            try {
	                modulusBytes = Base64Encoder.decode(MODULUS_CLAVE);
	                exponentBytes = Base64Encoder.decode(EXPONENT_CLAVE);
	                pBytes = Base64Encoder.decode(P_BYTES_CLAVE);
	                qBytes = Base64Encoder.decode(Q_BYTES_CLAVE);
	                dPBytes = Base64Encoder.decode(DP_BYTES_CLAVE);
	                dQBytes = Base64Encoder.decode(DQ_BYTES_CLAVE);
	                inverseQBytes = Base64Encoder.decode(INVERSE_BYTES_CLAVE);
	                dBytes = Base64Encoder.decode(D_BYTES_CLAVE);
	                modulus = new BigInteger(1, modulusBytes);
	                exponent = new BigInteger(1, exponentBytes);
	                fact = KeyFactory.getInstance("RSA");
	            } catch (NoSuchAlgorithmException e) {
					//TODO Arreglar
	                //LogManager.getInstance().error(CriptografiaRSA.class.getName(), "getInstance",e);
	            }
	            java.security.Security.addProvider(new BouncyCastleProvider());
	            return new CriptografiaRSA();
	        }
	    }
	    public String desencriptar(String s) throws Exception {
	        if (StringUtils.isEmpty(s)) {
	            return "";
	        } else {
	            BigInteger p = new BigInteger(1, pBytes);
	            BigInteger q = new BigInteger(1, qBytes);
	            BigInteger dP = new BigInteger(1, dPBytes);
	            BigInteger dQ = new BigInteger(1, dQBytes);
	            BigInteger inverseQ = new BigInteger(1, inverseQBytes);
	            BigInteger d = new BigInteger(1, dBytes);
	            RSAPrivateCrtKeySpec rsaPrivateKey = new RSAPrivateCrtKeySpec(modulus, exponent, d, p, q, dP, dQ, inverseQ);
	            PrivateKey privKey = fact.generatePrivate(rsaPrivateKey);
	            try {
	                cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC"); //BC no esta en el original
	                cipher.init(Cipher.DECRYPT_MODE, privKey);
	                String textoADescifrar = "";
	                String[] arrayTextoParticionado = s.split("=");
	                String textoSimpleADescifrar = "";
	                int largoCipherData = 0;
	                for (int i = 0; i < arrayTextoParticionado.length; i++) {
	                    if (arrayTextoParticionado[i].equalsIgnoreCase("")) {
	                        break;
	                    }
	                    textoSimpleADescifrar = arrayTextoParticionado[i] + "=";
	                    byte[] datosEnc = Base64Encoder.fromBase64String(textoSimpleADescifrar);
	                    byte[] cipherData2 = cipher.doFinal(datosEnc);
	                    largoCipherData = cipherData2.length;
	                    for (int j = 0; j < cipherData2.length; j++) {
	                        if (cipherData2[j] == 0) {//aca busco los ceros que me agrega de relleno y marco el lugar donde est�
	                            largoCipherData = j;
	                            break;
	                        }
	                    }
	                    byte[] cipherData = new byte[largoCipherData];
	                    for (int j = 0; j < largoCipherData; j++) {//aca saco el relleno de la cadena si es el caso
	                        cipherData[j] = cipherData2[j];
	                    }
	                    textoADescifrar += new String(cipherData);
	                }
	                return textoADescifrar;
	            } catch (Exception e) {
		        	//TODO Arreglar
	            	LoggerFactory.getLogger(CriptografiaRSA.class).error(e.getMessage(), e);
	                return "";
	            }
	        }
	    }
	    public String encriptar(String s) {
	        try {
	            RSAPublicKeySpec rsaPubKey = new RSAPublicKeySpec(modulus, exponent);
	            PublicKey pubKey = fact.generatePublic(rsaPubKey);
	            cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC"); //BC no esta en el original
	            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
	            String res = "";
	            byte[] plainBytes = s.getBytes();
	            if (plainBytes.length > BYTES) {
	                int cantidadDivisiones = (plainBytes.length / BYTES) + 1;
	                byte[] arrayTemporal;
	                for (int i = 1; i <= cantidadDivisiones; i++) {
	                    if (i - 1 == cantidadDivisiones) {
	                        arrayTemporal = new byte[plainBytes.length % BYTES];
	                    } else {
	                        arrayTemporal = new byte[BYTES];
	                    }
	                    for (int j = 0; j < BYTES; j++) {
	                        if (j + (BYTES * (i - 1)) < plainBytes.length) {
	                            arrayTemporal[j] = plainBytes[j + (BYTES * (i - 1))];
	                        } else {
	                            break;
	                        }
	                    }
	                    byte[] cipherData = cipher.doFinal(arrayTemporal);
	                    res = res + Base64Encoder.toBase64String(cipherData);
	                }
	            } else {
	                byte[] cipherData = cipher.doFinal(plainBytes);
	                res = res + new String(Base64Encoder.encode(cipherData));
	            }
	            return res;
	        } catch (Exception e) {
	            e.printStackTrace();
		        	//TODO Arreglar
	            //LogManager.getInstance().error(CriptografiaRSA.class.getName(), "encriptar",e);
	            return null;
	        }
	    }
//	    public static void main(String args[]) {
//	        CriptografiaRSA c = CriptografiaRSA.getInstance();
//	        String x = "RU69992";
//	        System.out.println(x);
//	        System.out.println(c.encriptar(x));
//	        try {
//				System.out.println(c.desencriptar("JDiyqf+nFE8N4GY+wCkC0kjnuaZ+KIHWCI6h2iImcYNFsaFJdUnBtcD11YfMPyBnEEXHqdTLyv9cWFONHY4Ke7ul4vWi5VJCPu2v88jrcQS1eJKMd5/3NKH9B+CdIZ9w6tNVq+FiCMmZda1s86GJRDSFUmalO7w+7Zm8Z413SPo="));
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//	    }
}