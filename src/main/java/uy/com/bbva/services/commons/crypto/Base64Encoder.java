package uy.com.bbva.services.commons.crypto;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
public class Base64Encoder {
	/**
	 * The BASE64 encoding standard's 6-bit alphabet, from RFC 1521, plus the
	 * padding character at the end.
	 */
	private static final char[] Base64Chars = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
			'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
			'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3',
			'4', '5', '6', '7', '8', '9', '+', '/', '=' };
	/**
	 * Encoding alphabet for session keys. Contains only chars that are safe to
	 * use in cookies, URLs and file names. Same as BASE64 except the last two
	 * chars and the padding char
	 */
	private static final char[] SessionKeyChars = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
			'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h',
			'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2',
			'3', '4', '5', '6', '7', '8', '9', '_', '-', '.' };
	/**
	 * Performs RFC1521 style Base64 encoding of arbitrary binary data. The
	 * output is a java String containing the Base64 characters representing the
	 * binary data. Be aware that this string is in Unicode form, and should be
	 * converted to UTF8 with the usual java conversion routines before it is
	 * sent over a network. The output string is guaranteed to only contain
	 * characters that are a single byte in UTF8 format. Also be aware that this
	 * routine leaves it to the caller to break the string into 70 byte lines as
	 * per RFC1521.
	 * 
	 * @param bytes
	 *            The array of bytes to convert to Base64 encoding.
	 * @return An string containing the specified bytes in Base64 encoded form.
	 */
	public static final String toBase64String(byte[] bytes) {
		return toBase64String(bytes, Base64Chars);
	}
	/**
	 * The encoding is more or less Base 64, but instead of '+' and '/' as
	 * defined in RFC1521, the characters '_' and '-' are used because they are
	 * safe in URLs and file names.
	 * 
	 * @param bytes
	 *            The array of bytes to convert to Base64SessionKey encoding.
	 * @return An string containing the specified bytes in Base64 encoded form.
	 */
	public static final String toBase64SessionKeyString(byte[] bytes) {
		return toBase64String(bytes, SessionKeyChars);
	}
	/**
	 * Performs encoding of arbitrary binary data based on a 6 bit alphabet. The
	 * output is a java String containing the encoded characters representing
	 * the binary data. Be aware that this string is in Unicode form, and should
	 * be converted to UTF8 with the usual java conversion routines before it is
	 * sent over a network. The alphabet passed in via <code>chars</code> is
	 * used without further checks, it's the callers responsibility to set it to
	 * something meaningful.
	 * 
	 * @param bytes
	 *            The array of bytes to convert to Base64 encoding.
	 * @param chars
	 *            The alphabet used in encoding. Must contain exactly 65
	 *            characters: A 6 bit alphabet plus one padding char at position
	 *            65.
	 * @return An string containing the specified bytes in Base64 encoded form.
	 */
	private static final String toBase64String(byte[] bytes, char[] chars) {
		StringBuffer sb = new StringBuffer();
		int len = bytes.length, i = 0, ival;
		while (len >= 3) {
			ival = ((int) bytes[i++] + 256) & 0xff;
			ival <<= 8;
			ival += ((int) bytes[i++] + 256) & 0xff;
			ival <<= 8;
			ival += ((int) bytes[i++] + 256) & 0xff;
			len -= 3;
			sb.append(chars[(ival >> 18) & 63]);
			sb.append(chars[(ival >> 12) & 63]);
			sb.append(chars[(ival >> 6) & 63]);
			sb.append(chars[ival & 63]);
		}
		switch (len) {
		case 0: // No pads needed.
			break;
		case 1: // Two more output bytes and two pads.
			ival = ((int) bytes[i++] + 256) & 0xff;
			ival <<= 16;
			sb.append(chars[(ival >> 18) & 63]);
			sb.append(chars[(ival >> 12) & 63]);
			sb.append(chars[64]);
			sb.append(chars[64]);
			break;
		case 2: // Three more output bytes and one pad.
			ival = ((int) bytes[i++] + 256) & 0xff;
			ival <<= 8;
			ival += ((int) bytes[i] + 256) & 0xff;
			ival <<= 8;
			sb.append(chars[(ival >> 18) & 63]);
			sb.append(chars[(ival >> 12) & 63]);
			sb.append(chars[(ival >> 6) & 63]);
			sb.append(chars[64]);
			break;
		}
		return new String(sb);
	}
	/**
	 * Performs RFC1521 style Base64 decoding of Base64 encoded data. The output
	 * is a byte array containing the decoded binary data. The input is expected
	 * to be a normal Unicode String object.
	 * 
	 * @param s
	 *            The Base64 encoded string to decode into binary data.
	 * @return An array of bytes containing the decoded data.
	 */
	public static final byte[] fromBase64String(String s) {
		try {
			StringCharacterIterator iter = new StringCharacterIterator(s);
			ByteArrayOutputStream bytestr = new ByteArrayOutputStream();
			DataOutputStream outstr = new DataOutputStream(bytestr);
			char c;
			int d, i, group;
			int[] bgroup = new int[4];
			decode: for (i = 0, group = 0, c = iter.first(); c != CharacterIterator.DONE; c = iter.next()) {
				switch (c) {
				case 'A':
					d = 0;
					break;
				case 'B':
					d = 1;
					break;
				case 'C':
					d = 2;
					break;
				case 'D':
					d = 3;
					break;
				case 'E':
					d = 4;
					break;
				case 'F':
					d = 5;
					break;
				case 'G':
					d = 6;
					break;
				case 'H':
					d = 7;
					break;
				case 'I':
					d = 8;
					break;
				case 'J':
					d = 9;
					break;
				case 'K':
					d = 10;
					break;
				case 'L':
					d = 11;
					break;
				case 'M':
					d = 12;
					break;
				case 'N':
					d = 13;
					break;
				case 'O':
					d = 14;
					break;
				case 'P':
					d = 15;
					break;
				case 'Q':
					d = 16;
					break;
				case 'R':
					d = 17;
					break;
				case 'S':
					d = 18;
					break;
				case 'T':
					d = 19;
					break;
				case 'U':
					d = 20;
					break;
				case 'V':
					d = 21;
					break;
				case 'W':
					d = 22;
					break;
				case 'X':
					d = 23;
					break;
				case 'Y':
					d = 24;
					break;
				case 'Z':
					d = 25;
					break;
				case 'a':
					d = 26;
					break;
				case 'b':
					d = 27;
					break;
				case 'c':
					d = 28;
					break;
				case 'd':
					d = 29;
					break;
				case 'e':
					d = 30;
					break;
				case 'f':
					d = 31;
					break;
				case 'g':
					d = 32;
					break;
				case 'h':
					d = 33;
					break;
				case 'i':
					d = 34;
					break;
				case 'j':
					d = 35;
					break;
				case 'k':
					d = 36;
					break;
				case 'l':
					d = 37;
					break;
				case 'm':
					d = 38;
					break;
				case 'n':
					d = 39;
					break;
				case 'o':
					d = 40;
					break;
				case 'p':
					d = 41;
					break;
				case 'q':
					d = 42;
					break;
				case 'r':
					d = 43;
					break;
				case 's':
					d = 44;
					break;
				case 't':
					d = 45;
					break;
				case 'u':
					d = 46;
					break;
				case 'v':
					d = 47;
					break;
				case 'w':
					d = 48;
					break;
				case 'x':
					d = 49;
					break;
				case 'y':
					d = 50;
					break;
				case 'z':
					d = 51;
					break;
				case '0':
					d = 52;
					break;
				case '1':
					d = 53;
					break;
				case '2':
					d = 54;
					break;
				case '3':
					d = 55;
					break;
				case '4':
					d = 56;
					break;
				case '5':
					d = 57;
					break;
				case '6':
					d = 58;
					break;
				case '7':
					d = 59;
					break;
				case '8':
					d = 60;
					break;
				case '9':
					d = 61;
					break;
				case '+':
					d = 62;
					break;
				case '/':
					d = 63;
					break;
				case '_':
					d = 62;
					break;
				case '-':
					d = 63;
					break;
				default:
					// Any character not in Base64 alphabet is treated
					// as end of data. This includes the '=' (pad) char.
					break decode; // Skip illegal characters.
				}
				bgroup[i++] = d;
				if (i >= 4) {
					i = 0;
					group = ((bgroup[0] & 63) << 18) + ((bgroup[1] & 63) << 12) + ((bgroup[2] & 63) << 6)
							+ (bgroup[3] & 63);
					outstr.writeByte(((group >> 16) & 255));
					outstr.writeByte(((group >> 8) & 255));
					outstr.writeByte(group & 255);
				}
			}
			// Handle the case of remaining characters and
			// pad handling. If input is not a multiple of 4
			// in length, then '=' pads are assumed.
			switch (i) {
			case 2:
				// One output byte from two input bytes.
				group = ((bgroup[0] & 63) << 18) + ((bgroup[1] & 63) << 12);
				outstr.writeByte(((group >> 16) & 255));
				break;
			case 3:
				// Two output bytes from three input bytes.
				group = ((bgroup[0] & 63) << 18) + ((bgroup[1] & 63) << 12) + ((bgroup[2] & 63) << 6);
				outstr.writeByte(((group >> 16) & 255));
				outstr.writeByte(((group >> 8) & 255));
				break;
			default:
				// Any other case, including correct 0, is treated as
				// end of data.
				break;
			}
			outstr.flush();
			return bytestr.toByteArray();
		} catch (IOException e) {
		} // Won't happen. Return null if it does.
		return null;
	}
	// *****************************************************************************************
	private static final String systemLineSeparator = System.getProperty("line.separator");
	// Mapping table from 6-bit nibbles to Base64 characters.
	private static char[] map1 = new char[64];
	static {
		int i = 0;
		for (char c = 'A'; c <= 'Z'; c++)
			map1[i++] = c;
		for (char c = 'a'; c <= 'z'; c++)
			map1[i++] = c;
		for (char c = '0'; c <= '9'; c++)
			map1[i++] = c;
		map1[i++] = '+';
		map1[i++] = '/';
	}
	// Mapping table from Base64 characters to 6-bit nibbles.
	private static byte[] map2 = new byte[128];
	static {
		for (int i = 0; i < map2.length; i++)
			map2[i] = -1;
		for (int i = 0; i < 64; i++)
			map2[map1[i]] = (byte) i;
	}
	/**
	 * Encodes a string into Base64 format. No blanks or line breaks are
	 * inserted.
	 * 
	 * @param s
	 *            A String to be encoded.
	 * @return A String containing the Base64 encoded data.
	 */
	public static String encodeString(String s) {
		return new String(encode(s.getBytes()));
	}
	/**
	 * Encodes a byte array into Base 64 format and breaks the output into lines
	 * of 76 characters. This method is compatible with
	 * <code>sun.misc.BASE64Encoder.encodeBuffer(byte[])</code>.
	 * 
	 * @param in
	 *            An array containing the data bytes to be encoded.
	 * @return A String containing the Base64 encoded data, broken into lines.
	 */
	public static String encodeLines(byte[] in) {
		return encodeLines(in, 0, in.length, 76, systemLineSeparator);
	}
	/**
	 * Encodes a byte array into Base 64 format and breaks the output into
	 * lines.
	 * 
	 * @param in
	 *            An array containing the data bytes to be encoded.
	 * @param iOff
	 *            Offset of the first byte in <code>in</code> to be processed.
	 * @param iLen
	 *            Number of bytes to be processed in <code>in</code>, starting
	 *            at <code>iOff</code>.
	 * @param lineLen
	 *            Line length for the output data. Should be a multiple of 4.
	 * @param lineSeparator
	 *            The line separator to be used to separate the output lines.
	 * @return A String containing the Base64 encoded data, broken into lines.
	 */
	public static String encodeLines(byte[] in, int iOff, int iLen, int lineLen, String lineSeparator) {
		int blockLen = (lineLen * 3) / 4;
		if (blockLen <= 0)
			throw new IllegalArgumentException();
		int lines = (iLen + blockLen - 1) / blockLen;
		int bufLen = ((iLen + 2) / 3) * 4 + lines * lineSeparator.length();
		StringBuffer buf = new StringBuffer(bufLen);
		int ip = 0;
		while (ip < iLen) {
			int l = Math.min(iLen - ip, blockLen);
			buf.append(encode(in, iOff + ip, l));
			buf.append(lineSeparator);
			ip += l;
		}
		return buf.toString();
	}
	/**
	 * Encodes a byte array into Base64 format. No blanks or line breaks are
	 * inserted in the output.
	 * 
	 * @param in
	 *            An array containing the data bytes to be encoded.
	 * @return A character array containing the Base64 encoded data.
	 */
	public static char[] encode(byte[] in) {
		return encode(in, 0, in.length);
	}
	/**
	 * Encodes a byte array into Base64 format. No blanks or line breaks are
	 * inserted in the output.
	 * 
	 * @param in
	 *            An array containing the data bytes to be encoded.
	 * @param iLen
	 *            Number of bytes to process in <code>in</code>.
	 * @return A character array containing the Base64 encoded data.
	 */
	public static char[] encode(byte[] in, int iLen) {
		return encode(in, 0, iLen);
	}
	/**
	 * Encodes a byte array into Base64 format. No blanks or line breaks are
	 * inserted in the output.
	 * 
	 * @param in
	 *            An array containing the data bytes to be encoded.
	 * @param iOff
	 *            Offset of the first byte in <code>in</code> to be processed.
	 * @param iLen
	 *            Number of bytes to process in <code>in</code>, starting at
	 *            <code>iOff</code>.
	 * @return A character array containing the Base64 encoded data.
	 */
	public static char[] encode(byte[] in, int iOff, int iLen) {
		int oDataLen = (iLen * 4 + 2) / 3; // output length without padding
		int oLen = ((iLen + 2) / 3) * 4; // output length including padding
		char[] out = new char[oLen];
		int ip = iOff;
		int iEnd = iOff + iLen;
		int op = 0;
		while (ip < iEnd) {
			int i0 = in[ip++] & 0xff;
			int i1 = ip < iEnd ? in[ip++] & 0xff : 0;
			int i2 = ip < iEnd ? in[ip++] & 0xff : 0;
			int o0 = i0 >>> 2;
			int o1 = ((i0 & 3) << 4) | (i1 >>> 4);
			int o2 = ((i1 & 0xf) << 2) | (i2 >>> 6);
			int o3 = i2 & 0x3F;
			out[op++] = map1[o0];
			out[op++] = map1[o1];
			out[op] = op < oDataLen ? map1[o2] : '=';
			op++;
			out[op] = op < oDataLen ? map1[o3] : '=';
			op++;
		}
		return out;
	}
	/**
	 * Decodes a string from Base64 format. No blanks or line breaks are allowed
	 * within the Base64 encoded input data.
	 * 
	 * @param s
	 *            A Base64 String to be decoded.
	 * @return A String containing the decoded data.
	 * @throws IllegalArgumentException
	 *             If the input is not valid Base64 encoded data.
	 */
	public static String decodeString(String s) {
		return new String(decode(s));
	}
	/**
	 * Decodes a byte array from Base64 format and ignores line separators, tabs
	 * and blanks. CR, LF, Tab and Space characters are ignored in the input
	 * data. This method is compatible with
	 * <code>sun.misc.BASE64Decoder.decodeBuffer(String)</code>.
	 * 
	 * @param s
	 *            A Base64 String to be decoded.
	 * @return An array containing the decoded data bytes.
	 * @throws IllegalArgumentException
	 *             If the input is not valid Base64 encoded data.
	 */
	public static byte[] decodeLines(String s) {
		char[] buf = new char[s.length()];
		int p = 0;
		for (int ip = 0; ip < s.length(); ip++) {
			char c = s.charAt(ip);
			if (c != ' ' && c != '\r' && c != '\n' && c != '\t')
				buf[p++] = c;
		}
		return decode(buf, 0, p);
	}
	/**
	 * Decodes a byte array from Base64 format. No blanks or line breaks are
	 * allowed within the Base64 encoded input data.
	 * 
	 * @param s
	 *            A Base64 String to be decoded.
	 * @return An array containing the decoded data bytes.
	 * @throws IllegalArgumentException
	 *             If the input is not valid Base64 encoded data.
	 */
	public static byte[] decode(String s) {
		return decode(s.toCharArray());
	}
	/**
	 * Decodes a byte array from Base64 format. No blanks or line breaks are
	 * allowed within the Base64 encoded input data.
	 * 
	 * @param in
	 *            A character array containing the Base64 encoded data.
	 * @return An array containing the decoded data bytes.
	 * @throws IllegalArgumentException
	 *             If the input is not valid Base64 encoded data.
	 */
	public static byte[] decode(char[] in) {
		return decode(in, 0, in.length);
	}
	/**
	 * Decodes a byte array from Base64 format. No blanks or line breaks are
	 * allowed within the Base64 encoded input data.
	 * 
	 * @param in
	 *            A character array containing the Base64 encoded data.
	 * @param iOff
	 *            Offset of the first character in <code>in</code> to be
	 *            processed.
	 * @param iLen
	 *            Number of characters to process in <code>in</code>, starting
	 *            at <code>iOff</code>.
	 * @return An array containing the decoded data bytes.
	 * @throws IllegalArgumentException
	 *             If the input is not valid Base64 encoded data.
	 */
	public static byte[] decode(char[] in, int iOff, int iLen) {
		if (iLen % 4 != 0)
			throw new IllegalArgumentException("Length of Base64 encoded input string is not a multiple of 4.");
		while (iLen > 0 && in[iOff + iLen - 1] == '=')
			iLen--;
		int oLen = (iLen * 3) / 4;
		byte[] out = new byte[oLen];
		int ip = iOff;
		int iEnd = iOff + iLen;
		int op = 0;
		while (ip < iEnd) {
			int i0 = in[ip++];
			int i1 = in[ip++];
			int i2 = ip < iEnd ? in[ip++] : 'A';
			int i3 = ip < iEnd ? in[ip++] : 'A';
			if (i0 > 127 || i1 > 127 || i2 > 127 || i3 > 127) {
				System.out.println("i0= " + i0 + " i1= " + i1 + " i2= " + i2 + " i3= " + i3);
				throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
			}
			int b0 = map2[i0];
			int b1 = map2[i1];
			int b2 = map2[i2];
			int b3 = map2[i3];
			if (b0 < 0 || b1 < 0 || b2 < 0 || b3 < 0) {
				System.out.println("i0= " + i0 + " i1= " + i1 + " i2= " + i2 + " i3= " + i3);
				System.out.println("b0= " + b0 + " b1= " + b1 + " b2= " + b2 + " b3= " + b3);
				throw new IllegalArgumentException("Illegal character in Base64 encoded data.");
			}
			int o0 = (b0 << 2) | (b1 >>> 4);
			int o1 = ((b1 & 0xf) << 4) | (b2 >>> 2);
			int o2 = ((b2 & 3) << 6) | b3;
			out[op++] = (byte) o0;
			if (op < oLen)
				out[op++] = (byte) o1;
			if (op < oLen)
				out[op++] = (byte) o2;
		}
		return out;
	}
	// Dummy constructor.
	private Base64Encoder() {
	}
}