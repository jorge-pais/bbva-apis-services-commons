package uy.com.bbva.services.commons.exceptions;
public class LoginException extends Exception {
    private String country;
    private String docType;
    private String docNumber;
    private String accountUsed;
    private String user;
    private String code;
    public LoginException(String country, String docType, String docNumber, String accountUsed, String user, String code) {
        this.country = country;
        this.docType = docType;
        this.docNumber = docNumber;
        this.accountUsed = accountUsed;
        this.user = user;
        this.code = code;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getDocType() {
        return docType;
    }
    public void setDocType(String docType) {
        this.docType = docType;
    }
    public String getDocNumber() {
        return docNumber;
    }
    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }
    public String getAccountUsed() {
        return accountUsed;
    }
    public void setAccountUsed(String accountUsed) {
        this.accountUsed = accountUsed;
    }
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }
}