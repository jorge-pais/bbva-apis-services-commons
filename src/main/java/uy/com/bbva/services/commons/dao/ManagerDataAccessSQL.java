package uy.com.bbva.services.commons.dao;
import com.jolbox.bonecp.BoneCPDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import uy.com.bbva.services.commons.config.ServicesCommonsConfiguration;
import uy.com.bbva.services.commons.utils.LogUtils;
import javax.annotation.PreDestroy;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 * Manejador de conexiones para base de datos SQL
 * Depende de la configuracion de poolSQL_Charrua{@link ServicesCommonsConfiguration#poolSQL_Charrua()} y poolSQL_CanalConfig{@link ServicesCommonsConfiguration#poolSQL_CanalConfig()}
 */
@Component
@RequestScope
public class ManagerDataAccessSQL extends ManagerDataAccess {
	@Autowired
	private BoneCPDataSource poolSQL_Charrua;
	@Autowired
	private BoneCPDataSource poolSQL_CanalConfig;
	@Autowired
	private LogUtils logger;
	private Connection connection_charrua;
	private Connection connection_canalconfig;
	private Connection getConnection(SQLDataBase dataBase){
		Connection connection = null;
		try {
			switch (dataBase){
				case CHARRUA:
					if(connection_charrua==null ||  connection_charrua.isClosed())
						connection_charrua = poolSQL_Charrua.getConnection();
					connection = connection_charrua;
					break;
				case CANAL_CONFIG:
					if(connection_canalconfig==null ||  connection_canalconfig.isClosed())
						connection_canalconfig = poolSQL_CanalConfig.getConnection();
					connection = connection_canalconfig;
					break;
			}
		}
		catch (Exception ex) {
			logger.logError(this.getClass().getName(), ex.getMessage(), "No se pudo conectar con la BD");
		}
		return connection;
	}
	public PreparedStatement prepareStatement(String sql,SQLDataBase dataBase) throws SQLException {
		Connection conn = this.getConnection(dataBase);
		return conn.prepareStatement(sql);
	}
	public CallableStatement prepareCall(String sql,SQLDataBase dataBase) throws SQLException {
		Connection conn = this.getConnection(dataBase);
		return conn.prepareCall(sql);
	}
	public void closeConnection(SQLDataBase dataBase) throws SQLException {
		this.close(this.getConnection(dataBase));
	}
	@PreDestroy
	public void cleanUp() throws Exception {
		this.close(connection_charrua);
		this.close(connection_canalconfig);
	}
}