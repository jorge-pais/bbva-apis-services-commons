package uy.com.bbva.services.commons.exceptions;
public class ServiceException extends Exception {
	private String internalMessage;
	public ServiceException(String message, String internalMessage, Exception e) {
		super(message, e);
		this.internalMessage = internalMessage;
	}
	public String getInternalMessage() {
		return internalMessage;
	}
	public void setInternalMessage(final String internalMessage) {
		this.internalMessage = internalMessage;
	}
}