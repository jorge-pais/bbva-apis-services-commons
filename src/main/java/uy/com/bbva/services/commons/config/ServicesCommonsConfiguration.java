package uy.com.bbva.services.commons.config;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;
import com.jolbox.bonecp.BoneCPDataSource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import uy.com.bbva.services.commons.crypto.CriptografiaRSA;

import java.sql.SQLException;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"uy.com.bbva.services"})
public class ServicesCommonsConfiguration {
    
    /**
     * Metodo para crear un datasources de BoneCP
     * @param url
     * @param driver
     * @param userEnc
     * @param passEnc
     * @param minConnections
     * @param maxConnections
     * @return
     */
    private BoneCPDataSource createBCPDataSources(String url, String driver, String userEnc, String passEnc, String minConnections, String maxConnections){
        BoneCPDataSource config = null;
        if (StringUtils.isNotEmpty(url)) {
            config = new BoneCPDataSource();
            config.setJdbcUrl(url);
            config.setDriverClass(driver);
            if (StringUtils.isNotEmpty(minConnections)) {
                config.setMinConnectionsPerPartition(Integer.parseInt(minConnections));
            }
            if (StringUtils.isNotEmpty(maxConnections)) {
                config.setMaxConnectionsPerPartition(Integer.parseInt(maxConnections));
            }
            try {
                config.setUser(CriptografiaRSA.getInstance().desencriptar(userEnc));
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                config.setPassword(CriptografiaRSA.getInstance().desencriptar(passEnc));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return config;
    }
    /**
     * Metodo para crear una configuracion de BoneCP
     * @param url
     * @param driver
     * @param userEnc
     * @param passEnc
     * @param minConnections
     * @param maxConnections
     * @return
     */
    private BoneCP createBCP(String url, String driver, String userEnc, String passEnc, String minConnections, String maxConnections){
        BoneCP boneCP = null;
        if (StringUtils.isNotEmpty(url)) {
            // load the database driver (make sure this is in your classpath!)
            try {
                Class.forName(driver);
                // setup the connection pool using BoneCP Configuration
                BoneCPConfig config = new BoneCPConfig();
                // jdbc url specific to your database, eg jdbc:mysql://127.0.0.1/yourdb
                config.setJdbcUrl(url);
                if (StringUtils.isNotEmpty(minConnections)) {
                    config.setMinConnectionsPerPartition(Integer.parseInt(minConnections));
                }
                if (StringUtils.isNotEmpty(maxConnections)) {
                    config.setMaxConnectionsPerPartition(Integer.parseInt(maxConnections));
                }
                try {
                    config.setUser(CriptografiaRSA.getInstance().desencriptar(userEnc));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    config.setPassword(CriptografiaRSA.getInstance().desencriptar(passEnc));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                // setup the connection pool
                boneCP = new BoneCP(config);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return boneCP;
    }
    @Value("${as400.url:#{null}}")
    private String as400_url;
    @Value("${as400.driver:com.ibm.as400.access.AS400JDBCDriver}")
    private String as400_driver;
    @Value("${as400.user:#{null}}")
    private String as400_userEnc;
    @Value("${as400.pass:#{null}}")
    private String as400_passEnc;
    @Value("${as400.min_connections:#{null}}")
    private String as400_minConnections;
    @Value("${as400.max_connections:#{null}}")
    private String as400_maxConnections;
    @Bean
    /**
     * Bean poolAs400 esta definido para configurar BoneCP
     * Para esto se necesitan en el aplicaction.properti las siguiente propiedades
     * as400.url = Por defecto hace que devuelva null ya que es un parametro requerido
     * as400.driver = Por defecto toma "com.ibm.as400.access.AS400JDBCDriver"
     * as400.user = Por defecto queda en blanco
     * as400.pass = Por defecto queda en blanco
     * as400.min_connections = Por defecto no se carga
     * as400.max_connections = Por defecto no se carga
     */
    public BoneCPDataSource poolAs400() throws SQLException, ClassNotFoundException {
        return createBCPDataSources(as400_url,as400_driver, as400_userEnc, as400_passEnc, as400_minConnections, as400_maxConnections);
    }
    @Value("${charrua.url:#{null}}")
    private String charrua_url;
    @Value("${charrua.driver:com.microsoft.sqlserver.jdbc.SQLServerDriver}")
    private String charrua_driver;
    @Value("${charrua.user:#{null}}")
    private String charrua_userEnc;
    @Value("${charrua.pass:#{null}}")
    private String charrua_passEnc;
    @Value("${charrua.min_connections:#{null}}")
    private String charrua_minConnections;
    @Value("${charrua.max_connections:#{null}}")
    private String charrua_maxConnections;
    @Bean
    /**
     * Bean poolSQL_Charrua esta definido para configurar BoneCP
     * Para esto se necesitan en el aplicaction.properti las siguiente propiedades
     * charrua.url = Por defecto hace que devuelva null ya que es un parametro requerido
     * charrua.driver = Por defecto toma "com.microsoft.sqlserver.jdbc.SQLServerDriver"
     * charrua.user = Por defecto queda en blanco
     * charrua.pass = Por defecto queda en blanco
     * charrua.min_connections = Por defecto no se carga
     * charrua.max_connections = Por defecto no se carga
     */
    public BoneCPDataSource poolSQL_Charrua() throws SQLException, ClassNotFoundException {
        return createBCPDataSources(charrua_url,charrua_driver, charrua_userEnc, charrua_passEnc, charrua_minConnections, charrua_maxConnections);
    }
    @Value("${canalconfig.url:#{null}}")
    private String canalconfig_url;
    @Value("${canalconfig.driver:com.microsoft.sqlserver.jdbc.SQLServerDriver}")
    private String canalconfig_driver;
    @Value("${canalconfig.user:#{null}}")
    private String canalconfig_userEnc;
    @Value("${canalconfig.pass:#{null}}")
    private String canalconfig_passEnc;
    @Value("${canalconfig.min_connections:#{null}}")
    private String canalconfig_minConnections;
    @Value("${canalconfig.max_connections:#{null}}")
    private String canalconfig_maxConnections;
    @Bean
    /**
     * Bean poolSQL_CanalConfig esta definido para configurar BoneCP
     * Para esto se necesitan en el aplicaction.properti las siguiente propiedades
     * canalconfig.url = Por defecto hace que devuelva null ya que es un parametro requerido
     * canalconfig.driver = Por defecto toma "com.microsoft.sqlserver.jdbc.SQLServerDriver"
     * canalconfig.user = Por defecto queda en blanco
     * canalconfig.pass = Por defecto queda en blanco
     * canalconfig.min_connections = Por defecto no se carga
     * canalconfig.max_connections = Por defecto no se carga
     */
    public BoneCPDataSource poolSQL_CanalConfig() throws SQLException, ClassNotFoundException {
        return createBCPDataSources(canalconfig_url,canalconfig_driver, canalconfig_userEnc, canalconfig_passEnc, canalconfig_minConnections, canalconfig_maxConnections);
    }
}