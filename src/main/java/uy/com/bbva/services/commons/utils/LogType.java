package uy.com.bbva.services.commons.utils;
public enum  LogType {
    BUSINESS("Business"),
    DEBUG("Debug"),
    ERROR("Error"),
    QUERY("Query"),
    LOGIN("Login"),
    MONITOREO("Monitoreo");
    private String value;
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    LogType(String value) {
        this.value = value;
    }
}