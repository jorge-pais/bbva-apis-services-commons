package uy.com.bbva.services.commons.utils;
public interface LogUtils {
    void logMonitoreo(final String className, final long timeRequest);
    void logDebug(final String className, final String message, final String internalMessage);
    void logQuery(final String className, final String sql, final long duration, final int rows);
    void logBusiness(final String className, final String message, final String internalMessage,
                     final String internalCode);
    void logError(final String className, final String message, final String internalMessage);
    void logLoginError(final String className, final String country, final String docType, final String docNumber, final String accountUsed, final String user, final String code);
    void logDebug(final String className, final String message, final String internalMessage, final Throwable throwable);
    void logQuery(final String className, final String sql, final long duration, final int rows, final Throwable throwable);
    void logBusiness(final String className, final String message, final String internalMessage,
                     final String internalCode, final Throwable throwable );
    void logError(final String className, final String message, final String internalMessage, final Throwable throwable);
    void logLoginError(final String className, final String country, final String docType, final String docNumber,
                       final String accountUsed, final String user, final String code, final Throwable throwable);
}