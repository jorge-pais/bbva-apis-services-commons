package uy.com.bbva.services.commons.dao;
import org.springframework.beans.factory.annotation.Autowired;
import uy.com.bbva.services.commons.utils.LogUtils;
import java.sql.*;
public abstract class ManagerDataAccess {
	public static final int NANO_TO_MILLIS = 1000000;
	@Autowired
	private LogUtils logger;
	public int getRowCount(ResultSet resultSet) throws SQLException {
		int numRows = 0;
//		resultSet.next();
//		// Go to the last row
//		resultSet.last();
//		numRows = resultSet.getRow();
//		// Reset row before iterating to get data
//		resultSet.beforeFirst();
		return numRows;
	}
	public boolean executeCallable(CallableStatement statement) throws SQLException {
		long startTime = System.nanoTime();
		final boolean result = statement.execute();
		long endTime = System.nanoTime();
		logger.logQuery(this.getClass().getName(), statement.toString(), (endTime - startTime)/NANO_TO_MILLIS, 0);
		return result;
	}
	public ResultSet executeQuery(PreparedStatement preparedStatement) throws SQLException {
		ResultSet resultSet;
		long startTime = System.nanoTime();
		resultSet = preparedStatement.executeQuery();
		long endTime = System.nanoTime();
		logger.logQuery(this.getClass().getName(), preparedStatement.toString(), (endTime - startTime)/NANO_TO_MILLIS, getRowCount(resultSet));
		return resultSet;
	}
	
	public int executeUpdate(PreparedStatement preparedStatement) throws SQLException {		
		long startTime = System.nanoTime();
		int result = preparedStatement.executeUpdate();
		long endTime = System.nanoTime();
		logger.logQuery(this.getClass().getName(), preparedStatement.toString(), (endTime - startTime)/NANO_TO_MILLIS, result);
		return result;
	}
	public void close(Statement statement) throws SQLException {
		if(statement!=null && !statement.isClosed()){
			statement.close();
		}
	}
	public void close(Connection connection) throws SQLException {
		if(connection!=null && !connection.isClosed()){
			connection.close();
		}
	}
}