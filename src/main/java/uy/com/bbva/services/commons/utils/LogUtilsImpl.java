package uy.com.bbva.services.commons.utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
/**
 * Clase encargada de loggear los mensajes con un formato definido
 * Esta por dejecto utiliza la implementacion de self4j y utiliza el logger apis
 */
@Component
public class LogUtilsImpl implements LogUtils{
	private Logger getLogger(){
		return LoggerFactory.getLogger("apis");
	}
	@Override
	public void logMonitoreo(final String className, final long timeRequest) {
		Logger LOG = getLogger();
		LOG.debug(LogUtilsConfig.getPayloadMonitoreo(), timeRequest);
	}
	@Override
	public void logDebug(final String className, final String message, final String internalMessage) {
		Logger LOG = getLogger();
		LOG.debug(LogUtilsConfig.getPayloadDebug(), message, internalMessage);
	}
	@Override
	public void logQuery(final String className, final String sql, final long duration, final int rows) {
		Logger LOG = getLogger();
		LOG.debug(LogUtilsConfig.getPayloadDebug(), sql, duration, rows);
	}
	@Override
	public void logBusiness(final String className, final String message, final String internalMessage,
			final String internalCode) {
		Logger LOG = getLogger();
		LOG.info(LogUtilsConfig.getPayloadBusiness(), message, internalMessage, internalCode);
	}
	@Override
	public void logError(final String className, final String message, final String internalMessage) {
		Logger LOG = getLogger();
		LOG.error(LogUtilsConfig.getPayloadError(), message, internalMessage);
	}
	@Override
	public void logLoginError(final String className, final String country, final String docType, final String docNumber, final String accountUsed, final String user, final String code) {
		Logger LOG = getLogger();
		LOG.error(LogUtilsConfig.getPayloadLoginError(), country, docType, docNumber, accountUsed, user, code);
	}
	@Override
	public void logDebug(final String className, final String message, final String internalMessage, final Throwable throwable) {
		Logger LOG = getLogger();
		LOG.debug(LogUtilsConfig.getPayloadDebug(), message, internalMessage, throwable);
	}
	@Override
	public void logQuery(final String className, final String sql, final long duration, final int rows, final Throwable throwable) {
		Logger LOG = getLogger();
		LOG.debug(LogUtilsConfig.getPayloadQuery(), sql, duration, rows, throwable);
	}
	@Override
	public void logBusiness(final String className, final String message, final String internalMessage,
								   final String internalCode, final Throwable throwable ) {
		Logger LOG = getLogger();
		LOG.info(LogUtilsConfig.getPayloadBusiness(), message, internalMessage, internalCode, throwable);
	}
	@Override
	public void logError(final String className, final String message, final String internalMessage, final Throwable throwable) {
		Logger LOG = getLogger();
		LOG.error(LogUtilsConfig.getPayloadError(), message, internalMessage, throwable);
	}
	@Override
	public void logLoginError(final String className, final String country, final String docType, final String docNumber,
									 final String accountUsed, final String user, final String code, final Throwable throwable) {
		Logger LOG = getLogger();
		LOG.error(LogUtilsConfig.getPayloadLoginError(), country, docType, docNumber, accountUsed, user, code, throwable);
	}
}