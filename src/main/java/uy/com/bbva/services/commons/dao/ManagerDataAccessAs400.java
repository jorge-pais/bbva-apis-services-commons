package uy.com.bbva.services.commons.dao;
import com.jolbox.bonecp.BoneCPDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;
import uy.com.bbva.services.commons.config.ServicesCommonsConfiguration;
import uy.com.bbva.services.commons.utils.LogUtils;
import javax.annotation.PreDestroy;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
/**
 * Manejador de conexiones para base de datos As400
 * Depende de la configuracion del poolAs400, esta se encuentra en {@link ServicesCommonsConfiguration#poolAs400()}
 */
@Component
@RequestScope
public class ManagerDataAccessAs400 extends ManagerDataAccess {
	@Autowired
	private BoneCPDataSource poolAs400;
	@Autowired
	private LogUtils logger;
	private Connection connection;
	protected Connection getConnection() {
		try {
			if(connection== null || connection.isClosed())
				connection = poolAs400.getConnection();
		}
		catch (Exception ex) {
			connection = null;
			logger.logError(this.getClass().getName(), ex.getMessage(), "No se pudo conectar con la BD");
		}
		return connection;
	}
	public CallableStatement prepareCall(String sql) throws SQLException {
		Connection conn = this.getConnection();
		return conn.prepareCall(sql);
	}
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		Connection conn = this.getConnection();
		return conn.prepareStatement(sql);
	}
	public void closeConnection() throws SQLException {
		close(getConnection());
	}
	@PreDestroy
	public void cleanUp() throws Exception {
		this.close(getConnection());
	}
}